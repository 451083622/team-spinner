package com.yunda.myapplication;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.yunda.teamspinner.localcomponent.SimpleEdit;
import com.yunda.teamspinner.localcomponent.doalog.ChooseDialog;
import com.yunda.teamspinner.localcomponent.entity.SpinnerBean;
import com.yunda.teamspinner.localcomponent.entity.TestSpinnerBean;
import com.yunda.teamspinner.localcomponent.sp_interface.OnRvItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName TestActivity
 *
 * @author 王玺权
 * date 2022-05-19 11:36
 * description
 * history
 */
public class TestActivity extends AppCompatActivity {
    private TextView tvValue;
    private  SimpleEdit tv1;
    private  SimpleEdit tv2;
    private ChooseDialog chooseDialog;
    private List<Object> spinnerList=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_test);
      tv1=findViewById(R.id.tv1);
      tv2=findViewById(R.id.tv2);
        tv1.setText("你好啊");
        tvValue=findViewById(R.id.tvValue);
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        spinnerList.add(new TestSpinnerBean("列表弹窗"));
        chooseDialog=ChooseDialog.getInstance(getSupportFragmentManager(), new OnRvItemClickListener() {
            @Override
            public void onRvItemClick(int position) {
                Toast.makeText(TestActivity.this,"点击的是:"+((TestSpinnerBean)spinnerList.get(position)).getItem(),Toast.LENGTH_LONG).show();
            }
        }).setTilte("标题");

         tvValue.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Toast.makeText(TestActivity.this,"右边输入值是"+tv1.getText()+"下方输入值是"+tv2.getText(),Toast.LENGTH_LONG).show();
                 chooseDialog.showDialog(spinnerList);

                 new Handler().postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         chooseDialog.dismiss();
                         spinnerList.clear();
                         spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                 spinnerList.add(new TestSpinnerBean("列表弹窗"));
                         chooseDialog.showDialog(spinnerList);
                     }
                 },3000);
             }
         });

    }
}

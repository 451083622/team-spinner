package com.yunda.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yunda.teamspinner.localcomponent.HeroBean;
import com.yunda.teamspinner.localcomponent.YdSpinner;
import com.yunda.teamspinner.localcomponent.sp_interface.ISpinnerItemValue;
import com.yunda.teamspinner.localcomponent.sp_interface.OnSpClickListener;
import com.yunda.teamspinner.localcomponent.sp_interface.OnSpItemClickListener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 王玺权
 * @date 2022年5月16日
 * @des 测试Spinner
 */
public class MainActivity extends AppCompatActivity {
    private TextView tvSelect;
    private YdSpinner<HeroBean> ydSpinner;
    private YdSpinner<HeroBean> ydSpinner2;
    private YdSpinner<HeroBean> ydSpinnerMulity;
    private YdSpinner<HeroBean> ydSpinnerMulity2;
    private YdSpinner<HeroBean> ydSpinnerSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvSelect=findViewById(R.id.tvSelect);
        ydSpinner=findViewById(R.id.ydSpinner);
        ydSpinner2=findViewById(R.id.ydSpinner2);
        ydSpinnerMulity2=findViewById(R.id.ydSpinnerMulity2);
        ydSpinnerMulity=findViewById(R.id.ydSpinnerMulity);
        ydSpinnerSearch=findViewById(R.id.ydSpinnerSearch);


        tvSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ydSpinner.reset();
                ydSpinnerMulity.reset();

                StringBuffer stringBuffer=new StringBuffer();
                List<HeroBean> checkBoxList = ydSpinner.getCheckData();
                for (int i = 0; i <checkBoxList.size() ; i++) {
                    stringBuffer.append(((ISpinnerItemValue)checkBoxList.get(i)).getItem());
                }

                StringBuffer stringBuffer1=new StringBuffer();
                List<HeroBean> checkBoxList1 = ydSpinnerMulity.getCheckData();
                for (int i = 0; i <checkBoxList1.size() ; i++) {
                    stringBuffer1.append(((ISpinnerItemValue)checkBoxList1.get(i)).getItem());
                }

                StringBuffer stringBuffer2=new StringBuffer();
                List<HeroBean> checkBoxList2 = ydSpinnerSearch.getCheckData();
                /**
                 * 选中数据
                 */
                Log.e("ydSpinnerSearch选中数据","??"+new Gson().toJson(checkBoxList2));
                for (int i = 0; i <checkBoxList2.size() ; i++) {
                    stringBuffer2.append(((ISpinnerItemValue)checkBoxList2.get(i)).getItem());
                }

                //单选2
                StringBuffer stringBuffers2=new StringBuffer();
                List<HeroBean> checkBoxLists2 = ydSpinner2.getCheckData();
                for (int i = 0; i <checkBoxLists2.size() ; i++) {
                    stringBuffers2.append(((ISpinnerItemValue)checkBoxLists2.get(i)).getItem());
                }

                //多选2
                StringBuffer stringBufferm2=new StringBuffer();
                List<HeroBean> checkBoxListm2 = ydSpinnerMulity2.getCheckData();
                for (int i = 0; i <checkBoxListm2.size() ; i++) {
                    stringBufferm2.append(((ISpinnerItemValue)checkBoxListm2.get(i)).getItem());
                }


//                ydSpinnerMulity.reset();


                Toast.makeText(MainActivity.this,"单选人物是："+stringBuffer.toString()+"-------\n多选人物是："+
                        stringBuffer1.toString() +"-------\n筛选人物是："+stringBuffer2.toString()
                                +"-------\n单选人物2是："+stringBuffers2.toString() +"-------\n多选人物2是："+stringBufferm2.toString()
                        ,Toast.LENGTH_LONG
                ).show();

//                ydSpinner.show();

            }
        });
        ydSpinner.setmData(getTestData());


        ydSpinner2.setmData(getTestData());
        ydSpinnerMulity2.setmData(getTestData());
        ydSpinnerMulity.setmData(getTestData());
        ydSpinnerSearch.setmData(getTestData());


//        ydSpinner.checkSingle(3);

        HeroBean heroBean = new HeroBean(R.drawable.ic_launcher_background, "赵云");
        ydSpinner.checkObj(heroBean);
        List<HeroBean> list=new ArrayList<>();
//        list.add(new HeroBean(R.drawable.ic_launcher_background, "赵云"));
//        list.add(new HeroBean(R.drawable.ic_launcher_background, "孙悟空"));
//        list.add(new HeroBean(R.drawable.ic_launcher_background, "百里玄策"));
        ydSpinnerMulity.checkListObj(list);


        ydSpinner.setOnSpClickListener(new OnSpClickListener() {
            @Override
            public void onSpClickListener(View v) {
                Toast.makeText(MainActivity.this,"点击回调",Toast.LENGTH_SHORT).show();
//                int pid= android.os.Process.myPid();
//                String s="ps -T -p"+pid;
//                execShell("adb devices");
//                Log.e("pid----",exec_Shell(s));
                ydSpinner.show();
            }
        });
        ydSpinnerMulity.setOnSpClickListener(new OnSpClickListener() {
            @Override
            public void onSpClickListener(View v) {
                ydSpinnerMulity.show();
            }
        });

        ydSpinner.setOnSpItemClickListener(new OnSpItemClickListener<HeroBean>() {
            @Override
            public void onSpItemClick(int position, List<HeroBean> list) {
                Log.e("点击数据","position="+position+"----list="+new Gson().toJson(list));
            }
        });

//        ydSpinner.setOnSpItemClickListener(new OnSpItemClickListener() {
//            @Override
//            public void onSpItemClick(int position, List<Object> list) {
//                Log.e("点击数据","position="+position+"----list="+new Gson().toJson(list));
//            }
//        });
//
//        ydSpinnerMulity.setOnSpItemClickListener(new OnSpItemClickListener() {
//            @Override
//            public void onSpItemClick(int position, List<Object> list) {
//                Log.e("点击数据","position="+position+"----list="+new Gson().toJson(list));
//            }
//        });
//
//        ydSpinnerSearch.setOnSpItemClickListener(new OnSpItemClickListener() {
//            @Override
//            public void onSpItemClick(int position, List<Object> list) {
//                Log.e("点击数据","position="+position+"----list="+new Gson().toJson(list));
//                execShell("cmd");
//            }
//        });
    }

    private List<HeroBean> getTestData(){
        List<HeroBean> mData=new ArrayList<>();
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"孙悟空"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"紫霞仙子"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"貂蝉"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"韩信"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"马超"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"赵云"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"赵子龙"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里百里玄策百里玄策百里玄策百里玄策玄策"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄百里玄策策1"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄策2"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄策3"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄策4"));
        mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄策6"));
        /*for (int i=0;i<=1000;i++){
            mData.add(new HeroBean(R.drawable.ic_launcher_background,"百里玄策"+i));
        }*/
        return mData;
    }

    public void execShell(String cmd){

        try{

//权限设置

            Process p = Runtime.getRuntime().exec("/system/bin/sh");

//获取输出流

            OutputStream outputStream = p.getOutputStream();

            DataOutputStream dataOutputStream=new DataOutputStream(outputStream);

//将命令写入

            dataOutputStream.writeBytes(cmd);

//提交命令

            dataOutputStream.flush();

//关闭流操作

            dataOutputStream.close();

            outputStream.close();

        }

        catch(Throwable t)

        {

            t.printStackTrace();

        }

    }

    public String exec_Shell(String cmd){
        StringBuilder s=new StringBuilder();
        try {
            Process p = Runtime.getRuntime().exec("/system/bin/sh");
            InputStream inputStream=p.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(
                  new InputStreamReader(inputStream),

                        p.waitFor()

                );


            String line=null;
            while ((line=bufferedReader.readLine())!=null){
                s.append(line).append("\n");
            }
            inputStream.close();
            bufferedReader.close();


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return s.toString();
    }

}
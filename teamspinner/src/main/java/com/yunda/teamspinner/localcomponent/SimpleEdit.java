package com.yunda.teamspinner.localcomponent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yunda.teamspinner.R;
import com.yunda.teamspinner.localcomponent.entity.SimpleEditType;

import java.util.zip.Inflater;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName SimpleEdit
 *
 * @author 王玺权
 * date 2022-05-19 10:32
 * description 通用的文本输入与展示框
 * history
 */
public class SimpleEdit extends LinearLayout {
    private String type = SimpleEditType.SIMPLE_TYPE;
    private TypedArray typedArray;
    /** 标题 */
    private String title;
    /** 提示 */
    private String hint;
    /** 能否输入 */
    private Boolean input=false;
    /** 文字颜色 */
    private Integer textColor=R.color.text666;
    private EditText viewSimpleRight;
    private EditText viewSimpleBottom;
    private TextView view_choose_tv_title;
    RelativeLayout rlRight;
    RelativeLayout rlBottom;
    public SimpleEdit(Context context) {
        super(context);
    }

    @SuppressLint("ResourceAsColor")
    public SimpleEdit(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_simpleedit, this, true);
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewEdit);
        type=typedArray.getString(R.styleable.ViewEdit_vp_editType);
        title=typedArray.getString(R.styleable.ViewEdit_vp_editTitle);
        hint=typedArray.getString(R.styleable.ViewEdit_vp_editHint);
        input=typedArray.getBoolean(R.styleable.ViewEdit_vp_editInput,false);
        textColor=typedArray.getColor(R.styleable.ViewEdit_vp_editTextColor,context.getResources().getColor(R.color.text666));
        viewSimpleRight=inflate.findViewById(R.id.viewSimpleRight);
        viewSimpleBottom=inflate.findViewById(R.id.viewSimpleBottom);
        view_choose_tv_title=inflate.findViewById(R.id.view_choose_tv_title);
        rlRight=inflate.findViewById(R.id.rlRight);
        rlBottom=inflate.findViewById(R.id.rlBottom);
        /** 按类型展示 */
        if(type.equals(SimpleEditType.SIMPLE_TYPE)){
            rlRight.setVisibility(VISIBLE);
            rlBottom.setVisibility(GONE);
        }else {
            rlRight.setVisibility(GONE);
            rlBottom.setVisibility(VISIBLE);
        }
        if(!input) {
            viewSimpleRight.setEnabled(false);
            viewSimpleBottom.setEnabled(false);
        }
        viewSimpleRight.setTextColor(textColor);
        viewSimpleBottom.setTextColor(textColor);
        view_choose_tv_title.setText(title);

        viewSimpleRight.setHint(hint);
        viewSimpleBottom.setHint(hint);
    }

    public void setText(String text){
        viewSimpleRight.setText(text);
        viewSimpleBottom.setText(text);
    }

    public String getText(){
        String text="";
        if(type.equals(SimpleEditType.SIMPLE_TYPE)){
            text=viewSimpleRight.getText().toString();
        }else {
            text=viewSimpleBottom.getText().toString();
        }
        return text;
    }
}

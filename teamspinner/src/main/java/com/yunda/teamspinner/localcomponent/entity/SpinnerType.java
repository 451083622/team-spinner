package com.yunda.teamspinner.localcomponent.entity;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName SpinnerType
 *
 * @author 王玺权
 * date 2022-05-14 10:17
 * description 下拉类型
 * history
 */
public class SpinnerType {
   /**
    * 常规下拉选择，包括单选多选
    */
   public static final String SELECT_TYPE ="1";
   /**
    * 搜索类型的下拉
    */
   public static final String SERCH_TYPE="2";
}

package com.yunda.teamspinner.localcomponent.sp_interface;

public interface OnRvItemClickListener {

    void onRvItemClick(int position);

}

package com.yunda.teamspinner.localcomponent.adapter;

import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.google.gson.Gson;
import com.yunda.teamspinner.R;
import com.yunda.teamspinner.localcomponent.popupwindow.YdPopWindow;
import com.yunda.teamspinner.localcomponent.sp_interface.ISpinnerItemValue;
import com.yunda.teamspinner.localcomponent.sp_interface.OnSpItemClickListener;
import com.yunda.util.CopyUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName SpinnerAdapter
 *
 * @author 王玺权
 * date 2022-05-13 16:55
 * description
 * history
 */
public class SpinnerAdapter<T extends ISpinnerItemValue> extends BaseQuickAdapter<T, BaseViewHolder> {
    private List<CheckBox> checkBoxList=new ArrayList<>();
    private String mType;
    private Boolean mIsSingle;
    private OnSpItemClickListener spItemClickListener;
    private List<T> heroBeanList=new ArrayList<>();
    private List<T> heroBeanListLocal=new ArrayList<>();
    private YdPopWindow<T> ydPopWindow;
    /**
     * 指定选中位置后需要手动设置位置
     */
    private Boolean needSetCheck=false;
    /**
     * 选中的位置和checkBoxList 对齐
     */
    private List<Integer> checkPositionList=new ArrayList<>();
    /**
     * 最新位置
     */
    private int latestPos=-1;
    private SparseBooleanArray mCheckStates=new SparseBooleanArray();

    public SpinnerAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }
    public SpinnerAdapter(int layoutResId, List<T> data, String type, Boolean isSingle, OnSpItemClickListener listener, YdPopWindow ydPopWindow) {
        super(layoutResId, data);
        this.mType=type;
        this.mIsSingle=isSingle;
        this.spItemClickListener=listener;
        this.ydPopWindow=ydPopWindow;
    }



    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder,T obj) {
        CheckBox cbName=baseViewHolder.getView(R.id.cbName);
        cbName.setTag(baseViewHolder.getAdapterPosition());



        ISpinnerItemValue heroBean= (ISpinnerItemValue) obj;
        TextView tvName=baseViewHolder.getView(R.id.tvName);

        LinearLayout llSpinner=baseViewHolder.getView(R.id.llSpinner);

        if(mIsSingle){
            cbName.setVisibility(View.GONE);
        }else {
            cbName.setVisibility(View.VISIBLE);
        }


        tvName.setText(heroBean.getItem());
        checkBoxList.add(cbName);
        llSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mIsSingle){
                    Log.e("选择类型","多选");
                    cbName.setChecked(!cbName.isChecked());
                    if(cbName.isChecked()){
                        heroBeanList.add((T) heroBean);
                    }else {
                        heroBeanList.remove(heroBean);
                    }
                    spItemClickListener.onSpItemClick(baseViewHolder.getAdapterPosition(),heroBeanList);
                    latestPos=baseViewHolder.getAdapterPosition();
                    heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
                }else {
                    Log.e("选择类型","单选");
                    if(latestPos>=0){
                        /**
                         * 点击的是自己
                         */
                        if(latestPos==baseViewHolder.getAdapterPosition()){
                            cbName.setChecked(true);
                        }else {
                            cbName.setChecked(!cbName.isChecked());
                            if(latestPos<getData().size()){
                                checkBoxList.get(latestPos).setChecked(false);
                            }
                            latestPos=baseViewHolder.getAdapterPosition();
                        }
                        latestPos=baseViewHolder.getAdapterPosition();
                    }else {
                        latestPos=baseViewHolder.getAdapterPosition();
                        cbName.setChecked(!cbName.isChecked());
                    }
                    heroBeanList.clear();
                    heroBeanList.add((T) heroBean);
                    heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
                    Log.e("点击返回数据是",new Gson().toJson(heroBeanListLocal)+"??");

                    spItemClickListener.onSpItemClick(baseViewHolder.getAdapterPosition(),heroBeanList);
                }

            }
        });

        cbName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("数据变化"+isChecked,"__"+isChecked);
                int pos= (int) buttonView.getTag();//得到当前CheckBox的Tag值，由于之前保存过，所以不会出现索引错乱
                if (isChecked){
                    //点击时将当前CheckBox的索引值和Boolean存入SparseBooleanArray中
                    mCheckStates.put(pos,true);
                }else {
                    //否则将 当前CheckBox对象从SparseBooleanArray中移除
                    mCheckStates.delete(pos);
                }
            }
        });

        cbName.setChecked(mCheckStates.get(baseViewHolder.getAdapterPosition(),false));
        if(cbName.isChecked()){
            System.out.println("==选中了=");
        }else{
            System.out.println("==取消了=");
        }
    }
    public List<T> getCheckBoxList(){

        Log.e("当前适配器数据",new Gson().toJson(heroBeanListLocal)+"?");



        Log.e("当前适配器数据2",new Gson().toJson(heroBeanList)+"?");

        heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
        Log.e("当前适配器数据3",new Gson().toJson(heroBeanListLocal)+"?");

        return heroBeanListLocal;
    }

    /**
     * 重置相关数据
     */
    public void resetData(){
        latestPos=-1;
        heroBeanList.clear();
        checkBoxList.clear();
        heroBeanListLocal.clear();
    }

    /**
     * 仅做为筛选
     */
    public void cleanData(){
//        latestPos=-1;
//        heroBeanList.clear();
//        checkBoxList.clear();
//        heroBeanListLocal.clear();
    }
    /**
     * 复原数据
     */
    public void recoveryData(){

        latestPos=-1;
        heroBeanList.clear();
//        checkBoxList.clear();
        heroBeanListLocal.clear();
//        checkPositionList.clear();
        mCheckStates.clear();
        checlAll(false);

        //将原数据复制并更新
        ydPopWindow.updateData(ydPopWindow.localData);
    }


    public void checkListPosition(List<Integer> position) {
        Log.e("列表消息","列表消息");
        needSetCheck=true;
        checkPositionList=position;
        heroBeanList.clear();
        for (int i = 0; i <position.size() ; i++) {
            heroBeanList.add((T) getData().get(position.get(i)));
            heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
            spItemClickListener.onSpItemClick(position.get(i),heroBeanList);
        }
        notifyDataSetChanged();
    }

    public void checkPosition(int position) {
        latestPos=position;
        for (int i = 0; i <checkBoxList.size() ; i++) {
            if(i==position){
                checkBoxList.get(i).setChecked(true);
            }else {
                checkBoxList.get(i).setChecked(false);
            }
        }
        heroBeanList.clear();
        heroBeanList.add((T) getData().get(position));
        heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
        Log.e("点击返回数据是",new Gson().toJson(heroBeanListLocal)+"??");

        spItemClickListener.onSpItemClick(position,heroBeanList);
        notifyDataSetChanged();
    }


    public void checkObj(T t) {
        for (int i = 0; i <getData().size() ; i++) {
            if(t.getItem().equals(getData().get(i).getItem())){
                latestPos=i;
            }
        }
           if(latestPos<0){
               return;
           }
        for (int i = 0; i <checkBoxList.size() ; i++) {
            if(i==latestPos){
                checkBoxList.get(i).setChecked(true);
            }else {
                checkBoxList.get(i).setChecked(false);
            }
        }
        heroBeanList.clear();
        heroBeanList.add((T) getData().get(latestPos));
        heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
        Log.e("点击返回数据是",new Gson().toJson(heroBeanListLocal)+"??");

        spItemClickListener.onSpItemClick(latestPos,heroBeanList);
        notifyDataSetChanged();
    }


    public void updateCheckList() {

        if(needSetCheck&&checkPositionList!=null){
            Log.e("长度",checkBoxList.size()+"--");
            for (int i = 0; i <checkPositionList.size() ; i++) {
               checkBoxList.get(checkPositionList.get(i)).setChecked(true);
            }
            needSetCheck=false;
        }
    }

    public void checkListObj(List<T> data) {

        needSetCheck=true;
        checkPositionList.clear();

        for (int i = 0; i <getData().size() ; i++) {
            for (int j = 0; j < data.size(); j++) {
                if(getData().get(i).getItem().equals(data.get(j).getItem())){
                    checkPositionList.add(i);
                }
            }
        }
        needSetCheck=true;
        heroBeanList.clear();
        for (int i = 0; i <checkPositionList.size() ; i++) {
            heroBeanList.add((T) getData().get(checkPositionList.get(i)));
            heroBeanListLocal= CopyUtil.deepCopy(heroBeanList);
            spItemClickListener.onSpItemClick(checkPositionList.get(i),heroBeanList);
        }
        notifyDataSetChanged();

//        notifyDataSetChanged();
    }


    /**
     * 全选，全部选
     * @param allChecked
     */
    public void checlAll(Boolean allChecked){
        for (int i = 0; i < checkBoxList.size(); i++) {
            checkBoxList.get(i).setChecked(allChecked);
        }
        notifyDataSetChanged();
    }
}

package com.yunda.teamspinner.localcomponent.entity;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName TestSpinnerBean
 *
 * @author 王玺权
 * date 2022-05-15 20:56
 * description
 * history
 */
public class TestSpinnerBean extends SpinnerBean{
    private int icon;
    private String name;

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public TestSpinnerBean(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getItem() {
        return name;
    }
}

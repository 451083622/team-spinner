package com.yunda.teamspinner.localcomponent.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yunda.teamspinner.R;
import com.yunda.teamspinner.localcomponent.entity.SpinnerBean;
import com.yunda.teamspinner.localcomponent.sp_interface.OnRvItemClickListener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName ChooseAdapter
 *
 * @author 王玺权
 * date 2022年5月19日
 * description
 * history
 */
public class ChooseAdapter extends BaseQuickAdapter<Object, BaseViewHolder> {
    private Boolean isChooseMan = false;
    private Boolean isSingle = false;
    public int currentPos=0;

    private LinearLayout item_choose_ll;
    private OnRvItemClickListener listener;
    public ChooseAdapter(int layoutResId, @Nullable List<Object> data) {
        super(layoutResId, data);
    }
    public ChooseAdapter(int layoutResId, @Nullable List<Object> data, OnRvItemClickListener listener) {
        super(layoutResId, data);
        this.listener=listener;
    }
    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Object object) {
      baseViewHolder.setText(R.id.item_choose_tv,((SpinnerBean)object).getItem());
      ImageView item_choose_img=baseViewHolder.getView(R.id.item_choose_img);
        item_choose_ll=baseViewHolder.findView(R.id.item_choose_ll);
        item_choose_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onRvItemClick(baseViewHolder.getAdapterPosition());
            }
        });

    }
    public void  setChooseMan(Boolean isChooseMan) {
        this.isChooseMan = isChooseMan;
    }
    public Boolean  getChooseMan() {
       return isChooseMan;
    }

    public void setSingle(Boolean isSingle) {
        this.isSingle = isSingle;
    }
}

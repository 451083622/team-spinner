package com.yunda.teamspinner.localcomponent.sp_interface;

import android.view.View;

import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName 点击回调不会直接展示
 *
 * @author 王玺权
 * date 2022-05-14 12:24
 * description
 * history
 */
public interface OnSpClickListener {
    /**
     * 点击事件
     */
    void onSpClickListener(View v);
}

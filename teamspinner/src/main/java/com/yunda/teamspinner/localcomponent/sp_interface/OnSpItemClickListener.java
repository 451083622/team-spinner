package com.yunda.teamspinner.localcomponent.sp_interface;

import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName OnSpItemClickLisener
 *
 * @author 王玺权
 * date 2022-05-14 12:24
 * description
 * history
 */
public interface OnSpItemClickListener<T extends ISpinnerItemValue> {
    /**
     * 点击事件
     * @param position
     * @param list
     */
    void onSpItemClick(int position, List<T> list);
}

package com.yunda.teamspinner.localcomponent.sp_interface;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName ISpinnerItemValue
 *
 * @author 王玺权
 * date 2022-05-16 10:16
 * description
 * history
 */
public interface ISpinnerItemValue {
    String getItem();
}

package com.yunda.teamspinner.localcomponent.doalog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yunda.teamspinner.R;
import com.yunda.teamspinner.localcomponent.adapter.ChooseAdapter;
import com.yunda.teamspinner.localcomponent.entity.SpinnerBean;
import com.yunda.teamspinner.localcomponent.sp_interface.ISpinnerItemValue;
import com.yunda.teamspinner.localcomponent.sp_interface.OnRvItemClickListener;
import com.yunda.util.ScreenUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName 弹窗
 *
 * @author 王玺权
 * date 2022年5月19日
 * description 选择框
 * history
 */
public class ChooseDialog extends DialogFragment {
    private  List<Object> stringList=new ArrayList<>();
    protected View mContainer;
    private FragmentManager manager;
    private LinearLayoutManager layoutManager;
    private RecyclerView dialog_choose_rv;
    private ChooseAdapter chooseAdapter;
    private OnRvItemClickListener clickListener;
    /**
     * 标题
     */
    private TextView dialog_choose_tv;
    private String title;
    private ChooseListener listener;
    private Button dialog_choose_btn;
    private Boolean isChoose=false;
    private Boolean isChooseMan=false;
    private Boolean checkSingle=false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public static ChooseDialog getInstance(FragmentManager manager, OnRvItemClickListener listener){
        return new ChooseDialog(manager,listener);
    }

    public ChooseDialog(FragmentManager manager, OnRvItemClickListener listener) {
        this.manager = manager;
        this.clickListener=listener;
    }
    public void setChooseManAndSingle(boolean isSingle) {
        if (mContainer != null) {
            chooseAdapter.setChooseMan(true);
            chooseAdapter.setSingle(isSingle);
        }else {
        }
       isChooseMan=true;
        checkSingle=isSingle;
    }
    public interface ChooseListener {
        void sure(List<SpinnerBean> chooseList);
    }
    public void setChooseListener(ChooseListener listener) {
        this.listener = listener;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (inflater == null) {
            return null;
        }
        setDialogAttributes(getDialog());
        mContainer = inflater.inflate(R.layout.dialog_view_choose, container, false);
        return mContainer;
    }
    public ChooseDialog setTilte(String tilte){
        this.title=tilte;
        return this;
    }
    protected void setDialogAttributes(Dialog dialog) {
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(getCanceledOnTouchOutside());
            dialog.setCancelable(getCancelable());
        }
    }
    protected boolean getCanceledOnTouchOutside() {
        return true;
    }
    protected boolean getCancelable() {
        return true;
    }

    @Override
    public void onInflate(@NonNull Context context, @NonNull AttributeSet attrs, @Nullable Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mContainer != null) {
            dialog_choose_tv=mContainer.findViewById(R.id.dialog_choose_tv);
            dialog_choose_tv.setText(title);
            dialog_choose_btn=mContainer.findViewById(R.id.dialog_choose_btn);
            dialog_choose_rv=mContainer.findViewById(R.id.dialog_choose_rv);
            layoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false);
            chooseAdapter=new ChooseAdapter(R.layout.item_choose,stringList,clickListener);
            dialog_choose_rv.setLayoutManager(layoutManager);
            dialog_choose_rv.setAdapter(chooseAdapter);

            getDialog().getWindow().setAttributes(getLayoutParams(getDialog().getWindow().getAttributes()));
            if(!isChoose){
                dialog_choose_btn.setVisibility(View.GONE);
            }
            chooseAdapter.setChooseMan(isChooseMan);
            chooseAdapter.setSingle(checkSingle);
            dialog_choose_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sure();
                }
            });
        }
    }

    public void setData(List<Object> data){
        stringList.clear();
        stringList.addAll(data);
        chooseAdapter.notifyDataSetChanged();
        if(chooseAdapter.getData().size()==0){
            chooseAdapter.setEmptyView(R.layout.layout_nodata_page);
        }
    }


    protected WindowManager.LayoutParams getLayoutParams(WindowManager.LayoutParams params) {
        if (params == null) {
            return new WindowManager.LayoutParams();
        }
        int width = ScreenUtil.getScreenWidth(getContext());
        int height = ScreenUtil.getScreenHeight(getContext());
        params.width = width - 200;
        params.height = (int)(height * 0.65);
        // 这里可以设置dialog布局的大小以及显示的位置
//        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        return params;
    }

    public void showDialog() {
        try {
            //在每个add事务前增加一个remove事务，防止连续的add
            manager.beginTransaction().remove(this).commit();
            super.show(manager, "");
        } catch (Exception e) {
            //同一实例使用不同的tag会异常，这里捕获一下
            e.printStackTrace();
        }
    }
    public void showDialog(List<Object> objects) {
        this.stringList=objects;
        try {
            //在每个add事务前增加一个remove事务，防止连续的add
            manager.beginTransaction().remove(this).commit();
            super.show(manager, "");
        } catch (Exception e) {
            //同一实例使用不同的tag会异常，这里捕获一下
            e.printStackTrace();
        }
    }
    @Override
    public int getTheme() {
        return R.style.CornersDialog;
    }

    private void sure() {
        List<SpinnerBean> data = new ArrayList<>();

        listener.sure(data);
    }
}

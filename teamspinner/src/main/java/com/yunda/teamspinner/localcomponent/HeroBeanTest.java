package com.yunda.teamspinner.localcomponent;



import com.yunda.teamspinner.localcomponent.entity.SpinnerBean;

import java.io.Serializable;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName HeroBean
 *
 * @author 王玺权
 * date 2022-05-13 16:40
 * description 测试实体
 * history
 */
public class HeroBeanTest extends SpinnerBean implements Serializable {
    private int icon;
    private String name;

    public HeroBeanTest() {
    }
    public HeroBeanTest(HeroBeanTest heroBean) {
    this.icon=heroBean.getIcon();
    this.name=heroBean.getName();
    }
    public HeroBeanTest(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void seName(String name) {
        this.name = name;
    }


    @Override
    public String getItem() {
        return name;
    }
}

package com.yunda.teamspinner.localcomponent.entity;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName SpinnerType
 *
 * @author 王玺权
 * date 2022年5月19日
 * description 文本类型
 * history
 */
public class SimpleEditType {
   /**
    * 右边输入框
    */
   public static final String SIMPLE_TYPE ="1";
   /**
    * 下边输入框
    */
   public static final String RIGHT_TYPE="2";

}

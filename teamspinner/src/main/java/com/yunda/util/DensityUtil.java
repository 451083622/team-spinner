package com.yunda.util;

import android.content.Context;

/**
 * 单位转换
 */
public class DensityUtil {

    /**
     * 将 dp 转化为 px
     */
    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
    /**
     * 将 dp 转化为 px
     */
    public static int dp2sp(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int i = (int) (dpValue * scale + 0.5f);
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (i / fontScale + 0.5f);
    }

    /**
     * 将 px 转化为 dp
     */
    public static int px2dp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将 px 转化为 sp
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将 sp 转化为 px
     */
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }



}

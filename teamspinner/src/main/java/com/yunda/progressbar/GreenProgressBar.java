package com.yunda.kmjx.widget.progressbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yunda.teamspinner.R;

/**
 * copyright (C), 2022, 运达科技有限公司
 * fileName GreenProgressBar
 *
 * @author 王玺权
 * date 2022-06-27 19:46
 * description
 * history
 */
public class GreenProgressBar extends RelativeLayout {
    private Context mContext;
    private TypedArray typedArray;
    private ProgressBar progressBar;
    private TextView tvProgress;
    /**
     * 进度
     */
    private int mProgress;
    public GreenProgressBar(Context context) {
        super(context);
    }

    public GreenProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_green_progressbar, this, true);
        this.mContext=context;
        progressBar=inflate.findViewById(R.id.my_progress);
        tvProgress=inflate.findViewById(R.id.tvProgress);
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.GreenProgressBar);
        mProgress=typedArray.getInteger(R.styleable.GreenProgressBar_gpb_progress,0);

        setText();
    }

    private void setText(){
        progressBar.setProgress(mProgress);
        tvProgress.setText(mProgress+"%");
        if(mProgress==100){
            tvProgress.setTextColor(getResources().getColor(R.color.white));
        }else {
            tvProgress.setTextColor(getResources().getColor(R.color.pg_green));
        }
    }

    public void setProgress(int progess){
       this.mProgress= progess;
       setText();
    }
}
